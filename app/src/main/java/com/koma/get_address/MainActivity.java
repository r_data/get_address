package com.koma.get_address;

import android.app.Activity;
import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.IOException;
import java.util.List;
import java.util.Locale;


public class MainActivity extends Activity {


    private EditText editTextAddress = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editTextAddress = (EditText) findViewById(R.id.editTextAddress);
        Button buttonGetAddress = (Button) findViewById(R.id.buttonGetAddress);

        buttonGetAddress.setOnClickListener(buttonGetAddressClickListener);

    }

    View.OnClickListener buttonGetAddressClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            Location lastKnownLocation = null;


            //lastKnownLocation = lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 5, this);
            lastKnownLocation = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);


            if (null != lastKnownLocation) {
                editTextAddress.setText(getAddressFromLocation(lastKnownLocation, Locale.KOREA));
            } else {
                editTextAddress.setText("STATUS ERROR!");
            }
        }

    };

    String getAddressFromLocation(Location location, Locale locale) {
        List<Address> addressList = null;
        Geocoder geocoder = new Geocoder(this, locale);

        try {
            addressList = geocoder.getFromLocation(
                    location.getLatitude(),
                    location.getLongitude(),
                    1
            );
        } catch (IOException e) {
            Toast.makeText(this, "NETWORK ERROR!", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
            return "STATUS ERROR";
        }
/*
            if (1 > addressList.size()) {
                return "STATUS ERROR";
            }
*/
        Address address = addressList.get(0);
        StringBuilder addressStringBuilder = new StringBuilder();
        for (int i = 0; i <= address.getMaxAddressLineIndex(); i++) {
            addressStringBuilder.append(address.getAddressLine(i));
            if (i < address.getMaxAddressLineIndex())
                addressStringBuilder.append("\n");
        }

        String address1 = addressStringBuilder.toString();

        String[] arrayOfString = address1.split(" ");

        String juso = (arrayOfString[1] + " " + arrayOfString[2] + " " + arrayOfString[3]);
        String juso_2 = arrayOfString[1];
        String juso_3 = arrayOfString[2];
        Log.d("Juso_2: ", juso_2);


        editTextAddress.setText(juso_2);

        return juso_2;
    }

}
